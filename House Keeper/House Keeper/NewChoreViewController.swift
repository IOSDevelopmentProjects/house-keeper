//
//  NewChoreViewController.swift
//  House Keeper
//
//  Created by Maximilian Hansson on 2018-11-14.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit
import CoreData

class NewChoreViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
	
	// MARK: - Properties
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var CategoryTextField: UITextField!
    @IBOutlet weak var OccurancePickerView: UIPickerView!
    @IBOutlet weak var StartDatePicker: UIDatePicker!
	
	var chores: [NSManagedObject] = []
    let occurrances = ["Daily", "Weekly", "Monthly"]
	var name = ""
	var category = ""
	var occurrence = 0
	var startDate = Date()
	
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return occurrances.count
    }
    

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return occurrances[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
		NameTextField.delegate = self
		CategoryTextField.delegate = self
        print("view did load, connected to controller")
		
    }
	
	// MARK: - Read chore info
    @IBAction func NewChoreButton(_ sender: Any) {
        name = NameTextField.text!
        
        category = CategoryTextField.text!
        
        let occuranceRow = OccurancePickerView.selectedRow(inComponent: 0)
        switch occuranceRow {
        case 0:
            occurrence = 1
        case 1:
            occurrence = 7
        case 2:
            occurrence = 30
        default:
            occurrence = 0
        }
        
        startDate = StartDatePicker.date
		
		//Save input to device
		self.saveChore(name: name, category: category, occurence: occurrence, startDate: startDate)
		
		//Go back to previous ViewController
		_ = navigationController?.popViewController(animated: true)
    }
	
	//MARK: - Save to CoreData
	func saveChore(name: String, category: String, occurence: Int, startDate: Date) {
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
		else {
			return
		}
		
		// 1
		let managedContext = appDelegate.persistentContainer.viewContext
		
		// 2
		let entity = NSEntityDescription.entity(forEntityName: "Chore", in: managedContext)!
		
		let chore = NSManagedObject(entity: entity, insertInto: managedContext)
		
		// 3
		chore.setValue(name, forKeyPath: "name")
		chore.setValue(category, forKeyPath: "category")
		chore.setValue(occurrence, forKeyPath: "occurrenceInDays")
		chore.setValue(startDate, forKeyPath: "startDate")
		
		// 4
		do {
			try managedContext.save()
			chores.append(chore)
		} catch let error as NSError {
			print("Could not save. \(error), \(error.userInfo)")
		}
	}
	
	// MARK: Keyboard functions
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == NameTextField {
			NameTextField.resignFirstResponder()
			CategoryTextField.becomeFirstResponder()
		} else {
			CategoryTextField.resignFirstResponder()
		}
		
		return true
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
}
