//
//  ChoreDetailsViewController.swift
//  House Keeper
//
//  Created by Daniel Tabacskó on 2018-11-30.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit

class ChoreDetailsViewController: UIViewController, UITextFieldDelegate {
	// MARK: - Properties
	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var category: UILabel!
	@IBOutlet weak var occurrence: UILabel!
	@IBOutlet weak var nextOccurrence: UILabel!
	
	var choreName: String = ""
	var choreCategory: String = ""
	var choreOccurrence: Int64 = 0
    var choreStartDate = Date()
	let date = Date()
	var isLater: Bool = false
	var nextOccuranceText = ""
	
	override func viewWillAppear(_ animated: Bool) {
		
		name.text = choreName
		category.text = choreCategory
		occurrence.text = occurrenceToString(occurrenceInt: choreOccurrence)
		
        if choreStartDate > date {
            isLater = true
        }
        let daysDifference = checkDaysDifference(start: choreStartDate, curr: date, islater: isLater)
        if isLater {
            nextOccuranceText = "In \(daysDifference) days"
            nextOccurrence.text = String(nextOccuranceText)
        } else {
            let daysLeft = calculateDaysLeft(daysDifference: daysDifference, occurr: choreOccurrence)
            nextOccuranceText = "In \(daysLeft) days"
            nextOccurrence.text = String(nextOccuranceText)
        }
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.title = choreName + " details"
    }
    
	func occurrenceToString(occurrenceInt: Int64) -> String {
		switch occurrenceInt {
		case 1:
			return "Daily"
		case 3:
			return "Every 3 days"
		case 7:
			return "Weekly"
		case 14:
			return "Biweekly"
		case 30:
			return "Monthly"
		default:
			return "Error"
		}
	}
    
    // MARK: - Segue functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChoreEditViewController {
            destination.choreName = choreName
            destination.choreCategory = choreCategory
            destination.choreOccurrence = choreOccurrence
            destination.choreStartDate = choreStartDate
        }
    }
}

// MARK: - Calculate next occurrence
func calculateDaysLeft(daysDifference: Int, occurr: Int64) -> Int{
    let rest = daysDifference % Int(occurr)
    let daysLeft = Int(occurr) - rest
    return daysLeft
}

func checkDaysDifference(start: Date, curr: Date, islater: Bool) -> Int{
    if islater {
        return (Calendar.current.dateComponents([.day], from: curr, to: start).day! + 1)
    } else {
        return Calendar.current.dateComponents([.day], from: start, to: curr).day!
    }
}
