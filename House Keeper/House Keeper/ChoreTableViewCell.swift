//
//  ChoreTableViewCell.swift
//  House Keeper
//
//  Created by Daniel Tabacskó on 2018-11-06.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit

class ChoreTableViewCell: UITableViewCell {
	@IBOutlet weak var choreLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
