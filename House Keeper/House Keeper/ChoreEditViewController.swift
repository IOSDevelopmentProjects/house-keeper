//
//  ChoreEditViewController.swift
//  House Keeper
//
//  Created by Alexander Linné on 2018-12-03.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit
import CoreData

class ChoreEditViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
	
	// MARK: - Properties
    var chores: [Chore] = []
    var choreName: String = ""
    var choreNameUpdated: String = ""
    var choreCategory: String = ""
    var choreCategoryUpdated: String = ""
    var choreOccurrence: Int64 = 0
    var choreOccurrenceUpdated: Int64 = 0
    var choreStartDate = Date()
    var choreStartDateUpdated = Date()
    let occurrances = ["Daily", "Weekly", "Monthly"]
    
    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var CategoryTextField: UITextField!
    @IBOutlet weak var OccurancePickerView: UIPickerView!
    @IBOutlet weak var StartDatePicker: UIDatePicker!
	
	// MARK: Save chore edits
    @IBAction func saveChore(_ sender: Any) {
        choreNameUpdated = NameTextField.text!
        choreCategoryUpdated = CategoryTextField.text!
        
        let occuranceRow = OccurancePickerView.selectedRow(inComponent: 0)
        switch occuranceRow {
        case 0:
            choreOccurrenceUpdated = 1
        case 1:
            choreOccurrenceUpdated = 3
        case 2:
            choreOccurrenceUpdated = 7
        case 3:
            choreOccurrenceUpdated = 14
        case 4:
            choreOccurrenceUpdated = 30
        default:
            choreOccurrenceUpdated = 0
        }
        choreStartDateUpdated = StartDatePicker.date
        
        //Save in CoreData
        updateChore(choreName: choreName, choreNameUpdated: choreNameUpdated, choreCategory: choreCategory, choreCategoryUpdated: choreCategoryUpdated, choreOccurrence: choreOccurrence, choreOccurrenceUpdated: choreOccurrenceUpdated, choreStartDate: choreStartDate, choreStartDateUpdated: choreStartDateUpdated)
        
        //Go back to previous ViewController
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return occurrances.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return occurrances[row]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NameTextField.text = choreName
        CategoryTextField.text = choreCategory
        
        var occuranceRow = 0
        switch choreOccurrence {
        case 1:
            occuranceRow = 0
        case 7:
            occuranceRow = 1
        case 30:
            occuranceRow = 2
        default:
            occuranceRow = 0
        }
        
        OccurancePickerView.selectRow(occuranceRow, inComponent: 0, animated: false)
        StartDatePicker.date = choreStartDate
    }

    override func viewDidLoad() {
        super.viewDidLoad()
		NameTextField.delegate = self
		CategoryTextField.delegate = self
    }
	
	// MARK: Keyboard functions
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == NameTextField {
			NameTextField.resignFirstResponder()
			CategoryTextField.becomeFirstResponder()
		} else {
			CategoryTextField.resignFirstResponder()
		}
		
		return true
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
}

// MARK: Update chore in CoreData
func updateChore(choreName: String, choreNameUpdated: String, choreCategory: String, choreCategoryUpdated: String, choreOccurrence: Int64, choreOccurrenceUpdated: Int64, choreStartDate: Date, choreStartDateUpdated: Date){
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Chore")
    fetchRequest.predicate = NSPredicate(format: "name = %@", choreName)
    
    do {
        let test = try managedContext.fetch(fetchRequest)
        
        let objectUpdate = test[0] as! NSManagedObject
        objectUpdate.setValue(choreNameUpdated, forKey: "name")
        objectUpdate.setValue(choreCategoryUpdated, forKey: "category")
        objectUpdate.setValue(choreOccurrenceUpdated, forKey: "occurrenceInDays")
        objectUpdate.setValue(choreStartDateUpdated, forKey: "startDate")
        do {
            try managedContext.save()
        } catch {
            print(error)
        }
    } catch {
        print(error)
    }
}
