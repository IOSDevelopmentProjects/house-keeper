//
//  StartTableViewController.swift
//  House Keeper
//
//  Created by Alexander Linné on 2018-11-22.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import CoreLocation

class StartTableViewController: UITableViewController, CLLocationManagerDelegate {
	// MARK: - IBAction for checkmarkbutton with animation
    @IBAction func checkMarkTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.25, delay: 0.1, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) { (success) in
            UIView.animate(withDuration: 0.25, delay: 0.1, options: .curveLinear, animations: {
                sender.isSelected.toggle()
                sender.transform = .identity
            }, completion: nil)
        }
    }
	
	// MARK: - Properties
	var chores: [Chore] = []
	var todaysChores: [Chore] = []
    var locationManager = CLLocationManager()
    let center =  UNUserNotificationCenter.current()

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
		// MARK: - Fetch CoreData
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext

        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Chore")

        //3
        do {
            chores = try managedContext.fetch(fetchRequest) as! [Chore]
            print("Fetched from local storage")
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        checkIfDueToday()
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Today"
        
        //MARK: Location handling
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()

        
        //MARK: Notification request
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (result, error) in
            //handle result of request failure
        }

		
        // For a cleaner table look.
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorStyle = .none
    }
	
	// MARK: - Todays chores
	func checkIfDueToday() {
		let date = Date()
		
		for chore in chores {
			var startDate = chore.startDate!
			let ocurrenceInDays = chore.occurrenceInDays
			var running = true
			
			while running {
				if(self.compareDates(date1: startDate, date2: date) == 2) {
					//Is more than todays date
					if todaysChores.contains(chore) {
						todaysChores = todaysChores.filter({$0 != chore})
					}
					running = false
				} else if(self.compareDates(date1: startDate, date2: date) == 1) {
					//Matches with the todays date
					if !todaysChores.contains(chore) {
						todaysChores.append(chore)
					}
					running = false
				}
				//Is less than todays date
				startDate = addNDays(date: startDate, addDays: Int(ocurrenceInDays))
			}
		}
	}
	
	func compareDates(date1: Date, date2: Date) -> Int {
		let compare = Calendar.current.compare(date1, to: date2, toGranularity: .day)
		switch compare {
		case .orderedSame:
			return 1
		case .orderedDescending:
			return 2
		default:
			return 0
		}
	}
	
	func addNDays(date: Date, addDays: Int) -> Date {
		let newDate = Calendar.current.date(byAdding: .day, value: addDays, to: date)
		return newDate!
	}
}

// EXTENSIONS
extension StartTableViewController {
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todaysChores.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StartTableViewCell", for: indexPath) as? StartTableViewCell {
            if todaysChores[indexPath.row].isFault {
                todaysChores.remove(at: indexPath.row)
                tableView.reloadData()
            } else {
                cell.choreLabel?.text = todaysChores[indexPath.row].name!
                cell.btnCheckBox.setImage(UIImage(named: "Checkmarkempty"), for: .normal)
                cell.btnCheckBox.setImage(UIImage(named: "Checkmark"), for: .selected)
                cell.btnCheckBox.tag = indexPath.row
            }
            return (cell)
        } else {
            return UITableViewCell()
        }
    }
}
