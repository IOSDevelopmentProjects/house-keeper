//
//  StartTableViewCell.swift
//  House Keeper
//
//  Created by Alexander Linné on 2018-11-22.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit

class StartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var choreLabel: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
