//
//  ChoreTableViewController.swift
//  House Keeper
//
//  Created by Daniel Tabacskó on 2018-11-06.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import CoreLocation

class ChoreTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    //MARK: Properties
    var chores: [Chore] = []
    var filteredChores: [Chore] = []
    let searchController = UISearchController(searchResultsController: nil)
    var locationManager = CLLocationManager()
    var location: CLLocation!
    let defaultLocation = CLLocation(latitude: 32, longitude: 32)
    let center =  UNUserNotificationCenter.current()
    let radiusInMeters = 100
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
		// MARK: - Fetch CoreData
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Chore")

        
        //3
        do {
            chores = try managedContext.fetch(fetchRequest) as! [Chore]
            print("Fetched from local storage")
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        //Sort alphabetically and reload table
        chores = chores.sorted() {$0.name!.lowercased() < $1.name!.lowercased()}
        self.tableView.reloadData()

    }
    
    override func viewDidLoad() {
        //TODO: Refresh after new entry
        super.viewDidLoad()
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Chores"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //MARK: Location handling
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
        location = locationManager.location ?? defaultLocation
        
        for chore in chores{
            sendNotifications(chore)
        }
    }

    // MARK: - Table view
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredChores.count
        }
        return chores.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "ChoreTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ChoreTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate chore for the data source layout.
        var chore = chores[indexPath.row]
        
        //Only show filtered results if searching
        if isFiltering() {
            chore = filteredChores[indexPath.row]
        } else {
            chore = chores[indexPath.row]
        }

        cell.choreLabel.text = chore.value(forKeyPath: "name") as? String
        
        return cell
    }
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "ShowDetails", sender: indexPath)
	}
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == UITableViewCell.EditingStyle.delete {
			let name: String = chores[indexPath.row].name!
			center.removePendingNotificationRequests(withIdentifiers: [chores[indexPath.row].name ?? ""])
			deleteChore(choreName: name)
			self.chores.remove(at: indexPath.row)
			tableView.reloadData()
		}
	}
    
    // MARK: - Functions for search functionallity
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredChores = chores.filter({( chore: Chore) -> Bool in
            return chore.name!.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    // MARK: - Segue functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChoreDetailsViewController {
            if let indexPath = sender as? IndexPath {

                let name = chores[indexPath.row].name!
                let category = chores[indexPath.row].category!
                let occurrence = chores[indexPath.row].occurrenceInDays
                let startDate = chores[indexPath.row].startDate
                destination.choreName = name
                destination.choreCategory = category
                destination.choreOccurrence = occurrence
                destination.choreStartDate = startDate!
                
            }
        }
    }
	
	// MARK: - Notifications
    func sendNotifications(_ chore :Chore){
        let content = UNMutableNotificationContent()
        content.title = chore.name ?? "known chore name"
        content.sound = UNNotificationSound.default
        
        var comps = DateComponents()
        
        if chore.occurrenceInDays==1{
            comps = Calendar.current.dateComponents([.hour, .minute], from: chore.startDate!)
        }else if chore.occurrenceInDays == 7{
            comps = Calendar.current.dateComponents([.weekday, .hour, .minute], from: chore.startDate!)
        }else if chore.occurrenceInDays == 30{
            comps = Calendar.current.dateComponents([.day, .hour, .minute], from: chore.startDate!)
        }else{
            comps = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: chore.startDate!)
        }

        comps.timeZone = Calendar.current.timeZone
        
        let trigger = UNCalendarNotificationTrigger.init(dateMatching: comps, repeats: true)
        let request = UNNotificationRequest(identifier: chore.name!, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("error in chore reminder: \(error.localizedDescription)")
            }
        }
		
		// MARK: - GPS functionallity
        let homeLocation = CLLocation(latitude: UserDefaults.standard.double(forKey: "Latitude"), longitude: UserDefaults.standard.double(forKey: "Longitude"))
        
        let distance = location.distance(from: homeLocation)
        
        if Int(distance) < radiusInMeters || CLLocationManager.authorizationStatus() == .denied{
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        } else{
            center.removePendingNotificationRequests(withIdentifiers: [chore.name!])
        }
    }
	
	// Disable notifications if not home
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location2 = locations.last {
            location = location2
            
            for chore in chores{
                sendNotifications(chore)
            }
        }
    }
}


extension ChoreTableViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    public func updateSearchResults(for searchController: UISearchController) {
    filterContentForSearchText(searchController.searchBar.text!)

    }
}

// MARK: - Delete from CoreData
func deleteChore(choreName: String){
            
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Chore")
    fetchRequest.predicate = NSPredicate(format: "name = %@", choreName)
    
    do {
        let chores = try managedContext.fetch(fetchRequest)
        
        let objectToDelete = chores[0] as! NSManagedObject
        managedContext.delete(objectToDelete)
        do {
            try managedContext.save()
        } catch {
            print(error)
        }
    } catch {
        print(error)
    }
}
