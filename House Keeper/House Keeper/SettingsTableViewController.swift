//
//  SettingsTableViewController.swift
//  House Keeper
//
//  Created by Jonatan Flodin Stenholm on 2018-11-27.
//  Copyright © 2018 Daniel Tabacskó. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class SettingsTableViewController: UITableViewController {
	
	// MARK: - Properties
    @IBOutlet var settingsTableView: UITableView!
    @IBOutlet weak var currentHomeLabel: UILabel!
    
    var placesClient: GMSPlacesClient!
    var location: CLLocation! //to store location
	
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsTableView.tableFooterView = UIView()
        placesClient = GMSPlacesClient.shared()
        
        if UserDefaults.standard.object(forKey: "HomeLocation") == nil{
            currentHomeLabel.text = "Current home not set"
        }else{
            let home = UserDefaults.standard.string(forKey: "HomeLocation")
            currentHomeLabel.text = home
        }
    }
	
	// MARK: - Set home location
    lazy var locationManager: CLLocationManager = {
        let _locationManager = CLLocationManager()
        _locationManager.requestWhenInUseAuthorization()
        // adjsut _locationManager
        return _locationManager
    }()
    
    @IBAction func setLocationButton(_ sender: Any) {
        
        let status = CLLocationManager.authorizationStatus()
		
		// Check users authorization
        switch status{
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            location = locationManager.location
            
            UserDefaults.standard.set(location.coordinate.latitude, forKey: "Latitude")
            UserDefaults.standard.set(location.coordinate.longitude, forKey: "Longitude")
            
            print(location.coordinate.latitude)
            
            placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                self.currentHomeLabel.text = "No current place"
                
                if let placeLikelihoodList = placeLikelihoodList {
                    let place = placeLikelihoodList.likelihoods.first?.place
                    if let place = place {
                        self.currentHomeLabel.text = place.formattedAddress?.components(separatedBy: ", ")
                            .joined(separator: "\n")
                        UserDefaults.standard.set(place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n"), forKey: "HomeLocation")
                    }
                }
            })
        case .denied:
            UserDefaults.standard.set(32, forKey: "Latitude")
            UserDefaults.standard.set(32, forKey: "Longitude")
            self.currentHomeLabel.text = "Allow GPS in your settings app to set location or check your internet connection"
            UserDefaults.standard.set("Allow GPS in your settings app to set location or check your internet connection", forKey: "homeLocation")
        case .notDetermined:
            UserDefaults.standard.set(32, forKey: "Latitude")
            UserDefaults.standard.set(32, forKey: "Longitude")
            self.currentHomeLabel.text = "Allow GPS in your settings app to set location or check your internet connection"
            UserDefaults.standard.set("Allow GPS in your settings app to set location or check your internet connection", forKey: "homeLocation")
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            location = locationManager.location
            
            UserDefaults.standard.set(location.coordinate.latitude, forKey: "Latitude")
            UserDefaults.standard.set(location.coordinate.longitude, forKey: "Longitude")
            
            print(location.coordinate.latitude)
            
            placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
                if let error = error {
                    print("Pick Place error: \(error.localizedDescription)")
                    return
                }
                
                self.currentHomeLabel.text = "No current place"
                
                if let placeLikelihoodList = placeLikelihoodList {
                    let place = placeLikelihoodList.likelihoods.first?.place
                    if let place = place {
                        self.currentHomeLabel.text = place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
                        UserDefaults.standard.set(place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n"), forKey: "HomeLocation")
                    }
                }
            })
        case .restricted:
            UserDefaults.standard.set(32, forKey: "Latitude")
            UserDefaults.standard.set(32, forKey: "Longitude")
            self.currentHomeLabel.text = "Allow GPS in your settings app to set location or check your internet connection"
            UserDefaults.standard.set("Allow GPS in your settings app to set location or check your internet connection", forKey: "homeLocation")
        
        }
    }
}
