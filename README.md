# House Keeper

Group project for the course "iOS Development"

We are going to develop an application that keeps track on all your chores. Daily, weekly and monthly.
The user will be able to make different lists with unique chores that you add yourself or pick from a list of chores that are already made from the developer. When adding the chores you have to tell the app how often you shall do this chore. If it is daily you will get a push-notification every day and so on.
Let’s say that you make a list for chores that is connected to your home, then you will have the opportunity to pin your location at your home so that you don’t get notifications about watering your plants when you are at work or in school (if that’s possible to implement). This is because if you get the notification at work, you most likely will hide it and forget it again before you get home.
It will be possible to create shopping lists aswell, that you can edit continuously.

### Standard chores
1. Remove the trash
2. Water plants
3. Dry-cleaning
4. Cleaning your home

### Customizable chores
###### Can be anything you like but, for example:
1. Go to bed (10 am daily)
2. Make your bed (7:30 am daily)
3. Call grandma (friday’s, weekly)
4. Practise (different days, weekly)